define

A classical Density Functional Theory python package for the study of 
inhomogeneous fluids. Calculations include interfacial tension, adsorption and
phase equilibrium.

The documentation can be found by clicking here: https://definedft.gitlab.io/definemanual/