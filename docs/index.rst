.. define documentation master file, created by
   sphinx-quickstart on Mon Jul 13 16:24:34 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to define's documentation!
==================================

This is the documentation of the python module define, which is a one-dimensional 
classical density functional theory implementation. It includes the calculation
of adsorption in slit-like pores and the calculation of interfacial tension. 
This package can be used for single and mulicomponent mixtures. 
This documentation describes the classes and functions that can be used to do
the calculations.

For a complete description of the installation we refer to the manual included 
in the git repository https://gitlab.com/definedft/definemanual

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   modules

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
